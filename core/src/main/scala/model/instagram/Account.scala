package model.instagram

case class Account(
    username:String,
    posts:Int,
    follower:Int,
    following:Int,
    description:String,
    postIdList:Seq[String]
                           )
