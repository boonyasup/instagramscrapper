package com.anymind

import com.anymind.scrapper.InstagramScrapperImpl

import scala.util.{Failure, Success}

object Main extends App {
  val scrapper = new InstagramScrapperImpl
//  val account = scrapper.getInstagramAccount("cristiano")
  val account = scrapper.getInstagramAccount("buanalinthip")
  account match {
    case Success(value) => println(s"success: $value")
    case Failure(e) => println(s"failure: ${e.getClass}")
    e.printStackTrace()
  }
}
