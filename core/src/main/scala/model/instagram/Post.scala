package model.instagram

import org.joda.time.DateTime

import scala.xml.Comment

case class Post(postDate:DateTime,
                imageLink:String,
                caption:String,
                hashtag: List[String],
                likedCount: Int,
                commentsCount:List[Comment])
