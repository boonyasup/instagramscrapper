package com.anymind.scrapper

import org.jsoup.nodes.{Document, Element}
import model.instagram
import scala.util.{Failure, Success, Try}
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.*
import org.openqa.selenium.By.{ByClassName, ByCssSelector, ByTagName, ByXPath, className}
import org.openqa.selenium.support.pagefactory.ByAll
import org.scalatestplus.selenium.WebBrowser
import org.openqa.selenium.JavascriptExecutor

import scala.annotation.tailrec
import scala.collection.mutable
import scala.io.Source
import scala.jdk.CollectionConverters.*


//class InstagramScrapperImpl extends InstagramScrapper with WebBrowser {

class InstagramScrapperImpl extends InstagramScrapper{
  override def getInstagramAccount(accountName:String):Try[instagram.Account] = {
    driverHandler(getUsingSeleniumDriver(accountName) _)
  }

  def driverHandler[A](f: WebDriver => instagram.Account): Try[instagram.Account] = {
    System.setProperty("webdriver.chrome.driver","/Users/panuwachboonyasup/Downloads/chromedriver")
    implicit val webDriver: WebDriver = new ChromeDriver
    Try {
      f(webDriver)
    } match {
      case r => webDriver.close()
      r
    }
  }

  def executeJavaScript(javascript: String, times: Int = 1, sleep: Int = 0)(implicit driver: WebDriver):Object = {
    if(driver.isInstanceOf[JavascriptExecutor])
    (1 to times).foldLeft(new Object)((_,_) => {
        val result = driver.asInstanceOf[JavascriptExecutor].executeScript(javascript)
        Thread.sleep(sleep)
        result
    })
    else throw new IllegalStateException("This driver does not support JavaScript!")
  }


  def getUsingSeleniumDriver(accountName:String)(driver: WebDriver): instagram.Account = {
    implicit val dv = driver
    val home = "https://www.instagram.com/accounts/login/"
    val userpage = s"https://www.instagram.com/$accountName/"
    println(s"connect to host: $userpage")

    val username = "tellyanymind"
    val email = "tanymind@yahoo.com"
    val password = "P_ssw1rd"
    val input = ""

//    driver.get(home)
//    Thread.sleep(1000)
//    driver.findElement(new ByCssSelector("input[name=username]")).sendKeys(username)
//    driver.findElement(new ByCssSelector("input[name=password]")).sendKeys(password)
//    driver.findElements(new ByCssSelector("button")).asScala.filter(x => x.getAttribute("innerHTML").contains("Log In</div>")).head.click()
//
//
//    Thread.sleep(5000)
    executeJavaScript("window.scrollTo(0,document.body.scrollHeight);", 5, 700)
    val result = parseUserInfo(driver.getPageSource)
    Thread.sleep(60*5000)
    driver.get(userpage)

    result
  }

  def parseDisplayNumber(text:String): Option[Int] = {
    val removedCommas = text.split(" ").head.replaceAll(",","").toLowerCase()
    println(removedCommas.slice(0,removedCommas.length-1))
    Try (
      if(removedCommas.endsWith("k")){
        removedCommas.slice(0,removedCommas.length-1).toDouble * 1000
      } else if (removedCommas.endsWith("m")) {
        removedCommas.slice(0,removedCommas.length-1).toDouble * 1000000
      } else {
        removedCommas.replaceAll("[^\\d]","").toInt
      }
    ).toOption.map(_.toInt)
  }

  def parseUserInfo(html:String): instagram.Account = {
  import org.jsoup.Jsoup
    val browser = Jsoup.parse(html)
    val follower = browser.getElementsByTag("li").asScala.find(hasTextUnder(_, "followers"))
    val following = browser.getElementsByTag("li").asScala.find(hasTextUnder(_, "following"))
    val post = browser.getElementsByTag("li").asScala.find(hasTextUnder(_, "post"))
    val followingCount = following.flatMap(text => parseDisplayNumber(text.child(0).tagName("span").text()))
    val followerCount = follower.flatMap(text => parseDisplayNumber(text.child(0).children().asScala.filter(_.tagName() == "span").head.attributes().get("title")))
    val postCount = post.flatMap(text => parseDisplayNumber(text.tagName("span").text().replaceAll("[^\\d]","")))
    val bioSection = browser.getElementsByTag("section").asScala.find(hasTagNameAbove(_, "header"))
    val name = bioSection.map(_.children().asScala.filter(_.tagName() == "div")(0).getElementsByTag("h2").text())
    val bio = bioSection.map(_.children().asScala.filter(_.tagName() == "div")(1).text())
    val postList = getPostList(browser)
    instagram.Account(
      name.getOrElse(""),
      postCount.getOrElse(0),
      followerCount.getOrElse(0),
      followingCount.getOrElse(0),
      bio.getOrElse(""),
      postList
    )
  }

  def getPostList(document: Document): Seq[String] = {
    val classNames = document.getElementsByTag("div").asScala.filter(div => hasNLevelChild(div,3) && hasTagNameUnder(div,"a") && hasTagNameUnder(div,"img")).groupBy(_.className()).toSeq
    val sortedClassNames = classNames.sorted((a:(String,mutable.Buffer[Element]), b:(String,mutable.Buffer[Element])) => b._2.size - a._2.size)
    val postItemsGroupByCount = sortedClassNames.groupBy(_._2.size)
    val maxCountDiv: (Int, Seq[(String, mutable.Buffer[Element])]) = postItemsGroupByCount.max((a:(Int, Seq[(String, mutable.Buffer[Element])]), b:(Int, Seq[(String, mutable.Buffer[Element])])) => a._1 - b._1)
    println(maxCountDiv)
    val finalDiv = maxCountDiv._2.map(_._2)
    val postLink = finalDiv.sorted((a:mutable.Buffer[Element],b:mutable.Buffer[Element]) => getNumberOfChildLevel(b.head) - getNumberOfChildLevel(a.head))
    postLink(0).map(_.child(0).attributes().get("href")).toSeq
  }

  @tailrec
  private def getNumberOfChildLevel(elem:Element, count:Int = 0):Int = {
    if(elem.children().size() > 0) getNumberOfChildLevel(elem.child(0), count + 1)
    else count
  }

  @tailrec
  private def hasNLevelChild(elem:Element, n:Int, count:Int = 0):Boolean = {
    if (count == n && elem.children().size() > 0) true
    else if(elem.children().size() > 0) hasNLevelChild(elem.child(0), n, count + 1)
    else false
  }

  @tailrec
  private def hasTagNameUnder(elem:Element, tagName:String):Boolean = {
    if(elem.tagName() == tagName) true
    else if(elem.children().size() > 0) hasTagNameUnder(elem.child(0),tagName)
    else false
  }

  @tailrec
  private def hasTagNameAbove(elem:Element, tagName:String):Boolean = {
    if(elem.tagName() == tagName) true
    else if(elem.parent() != null) hasTagNameAbove(elem.parent(), tagName)
    else false
  }

  @tailrec
  private def hasTextUnder(elem:Element, text:String):Boolean = {
    if(elem.text().contains(text)) true
    else if(elem.children().size() > 0) hasTextUnder(elem.child(0), text)
    else false
  }
}

object JustRun extends App {
  val sc = new InstagramScrapperImpl
  println(sc.parseDisplayNumber("100,761,170"))
}