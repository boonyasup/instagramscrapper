package model.instagram

import org.joda.time.DateTime

sealed trait Comment {
  def likeCount: Int
  def text: String
  def timestamp: DateTime
}
case class HeadComment(likeCount:Int,
                       text:String,
                       timestamp: DateTime,
                       subComment: List[SubComment]
                      ) extends Comment

case class SubComment(likeCount:Int,
                      text:String,
                      timestamp: DateTime) extends Comment