import Dependencies.*
import sbt.Keys.libraryDependencies

ThisBuild / scalaVersion     := "2.13.7"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.anymind"
ThisBuild / organizationName := "example"

ThisBuild / Compile / scalacOptions ++= Seq(
  "-target:11",
  "-deprecation",
  "-feature",
  "-unchecked",
  "-Xlog-reflective-calls",
  "-Ymacro-annotations",
  "-Xlint:deprecation",
  "-Xlint:nullary-unit",
  "-Xsource:3"
)


lazy val core = (project in file("core"))
  .settings(
    name := "instagram_scrapper",
//    unmanagedBase := baseDirectory.value / "lib",
    libraryDependencies ++=
      Seq(
        "net.ruippeixotog" %% "scala-scraper" % "2.2.1",
        "org.jsoup" % "jsoup" % "1.14.3",
        "org.scalatestplus" %% "selenium-3-141" % "3.2.7.0" ,
        "org.seleniumhq.selenium" % "selenium-chrome-driver" % "3.141.59"
      )
  )


lazy val root = (project in file("."))
  .settings(
    name := "instagram_scrapper",
    libraryDependencies ++=
      Seq(
      "net.ruippeixotog" %% "scala-scraper" % "2.2.1",
      "org.jsoup" % "jsoup" % "1.14.3",
      "org.scalatestplus" %% "selenium-3-141" % "3.2.7.0" ,
      "org.seleniumhq.selenium" % "selenium-chrome-driver" % "3.141.59"
    )
  ).dependsOn(core).aggregate(core)


