package com.anymind.scrapper

import model.instagram

import scala.util.Try

trait InstagramScrapper {
  def getInstagramAccount(accountName:String):Try[instagram.Account]
}
